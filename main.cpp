#include "Vector.h"
#include "Color.h"
#include "Ray.h"
#include "Scene.h"
#include "Camera.h"
#include <assert.h>
#include <stdio.h>
#include <fstream>

using namespace std;

void test_Vector() {
    Vector3F x1(1, 2, 3);
    assert(x1 == Vector3F(1, 2, 3));
    assert(x1 * 3 == Vector3F(3, 6, 9));
    assert(3 * x1 == Vector3F(3, 6, 9));
    assert(x1 / 1 == Vector3F(1, 2, 3));
    assert(x1.magnitude() == sqrtf(14));
    assert(x1.squared_magnitude() == 14);
    assert(x1.normalized() == x1 / x1.magnitude());
    x1 += Vector3F(2, 2, 2);
    assert(x1 == Vector3F(3, 4, 5));
    x1 -= Vector3F(1, 3, 4);
    assert(x1 == Vector3F(2, 1, 1));
    x1 /= 1;
    assert(x1 == Vector3F(2, 1, 1));
    x1 *= 1;
    assert(x1 == Vector3F(2, 1, 1));
    x1 = Vector3F(1, 2, 3);
    assert(x1 == Vector3F(1, 2, 3));

    Vector3F y1(2, 3, 4);
    assert(x1 + y1 == Vector3F(3, 5, 7));
    assert(x1 - y1 == Vector3F(-1, -1, -1));
    assert(x1 * y1 == 20);
    assert(x1 % y1 == Vector3F(-1, 2, -1));

    printf("All tests for class Vector3F passed!");
}

void test_Color() {
    Color c1(1, 1, 1);
    assert(c1 == Color(1, 1, 1));
    assert(c1.get_red() == 1);
    assert(c1.get_blue() == 1);
    assert(c1.get_green() == 1);
    assert(c1 * 1 == Color(1, 1, 1));
    assert(1 * c1 == Color(1, 1, 1));
    assert(c1 / 1 == Color(1, 1, 1));
    c1 += Color(1, 1, 1);
    assert(c1 == Color(2, 2, 2));
    c1 -= Color(1, 1, 1);
    assert(c1 == Color(1, 1, 1));
    c1 *= 1;
    assert(c1 == Color(1, 1, 1));
    c1 /= 1;
    assert(c1 == Color(1, 1, 1));
    c1 = Color(1, 1, 1);
    assert(c1 == Color(1, 1, 1));
    
    Color c2(2, 2, 2);
    assert(c1 + c2 == Color(3, 3, 3));
    assert(c1 - c2 == Color(-1, -1, -1));
    assert(c1 * c2 == Color(2, 2, 2));
    
    printf("All tests for class Color passed!");
}

void test_Ray() {
    Ray r1(Vector3F(1, 2, 3), Vector3F(1, 1, 1));
    assert(r1.get_direction() == Vector3F(1, 1, 1).normalized());
    assert(r1.get_origin() == Vector3F(1, 2, 3));
    assert(r1.get_point_at_t(0) == Vector3F(1, 2, 3));
    Ray r2(Vector3F(1, 2, 3), Vector3F(1, 1, 1), false);
    assert(r2.get_point_at_t(2) == Vector3F(3, 4, 5));
    Ray r3(Vector3F(1, 2, 3), Vector3F(1, 0, 0));
    assert(r3.get_point_at_t(5) == Vector3F(6, 2, 3));

    printf("All tests for class Ray passed!");
}

void test_SceneObject() {

};

void test_Scene() {

};

void test() {
    test_Ray();
}

void render_sphere() {
    fstream file("/Users/rogerchen/test.ppm");
    // fstream file("C:\\Users\\RogerChern\\Desktop\\test.ppm");
    assert(file.is_open());
    Scene scene;
    scene.add_light(make_shared<Light>(Vector3F(-10, 10, 5), Color(1, 1, 1)));
    scene.add_light(make_shared<Light>(Vector3F(5, 3, 5), Color(1, 1, 1)));
    shared_ptr<SceneObject> sphere1 = make_shared<Sphere>(0.5, Vector3F(0, 0.5, 0));
    sphere1->set_color(Color(0, 0, 0));
    sphere1->set_reflectivity(1);
    scene.add_scene_object(sphere1);
//    shared_ptr<SceneObject> sphere2 = make_shared<Sphere>(0.5, Vector3F(1.2, 0.5, 0));
//    sphere2->set_color(Color(0, 1, 0));
//    sphere2->set_reflectivity(0.7);
//    scene.add_scene_object(sphere2);
//    shared_ptr<SceneObject> sphere3 = make_shared<Sphere>(0.5, Vector3F(-1.2, 0.5, 0));
//    sphere3->set_color(Color(1, 0, 0));
//    sphere3->set_reflectivity(0.7);
//    scene.add_scene_object(sphere3);
    shared_ptr<SceneObject> plane1 = make_shared<Plane>(0, Vector3F(0, 1, 0));
    plane1->set_color(Color(0.65, 0, 0));
    plane1->set_reflectivity(0.1);
    scene.add_scene_object(plane1);
    
    Camera camera(Vector3F(-1.5, 1, 3), Vector3F(-0.3, 0.5, 0), Vector3F(0, 1, 0));
    scene.render(camera, 1000, file);
}

int main() {
    render_sphere();
    return 0;
}