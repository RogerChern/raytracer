//
// Created by RogerChern on 2015/5/11.
//

#ifndef RAYTRACER_COLOR_H
#define RAYTRACER_COLOR_H

#include <iostream>
using std::ostream;

class Color {
    friend Color operator + (const Color &lhs, const Color &rhs);
    friend Color operator - (const Color &lhs, const Color &rhs);
    friend Color operator * (const Color &lhs, const Color &rhs);
    friend Color operator * (const Color &lhs, float scalar);
    friend Color operator * (float scalar, const Color &rhs);
    friend Color operator / (const Color &lhs, float scalar);
    friend ostream & operator << (ostream &out, const Color &rhs);

private:
    float red = 0;
    float blue = 0;
    float green = 0;
public:
    Color() = default;
    Color(float red, float blue, float green) : red(red), blue(blue), green(green) {}
    float get_red() {
        return red;
    }
    float get_blue() {
        return blue;
    }
    float get_green() {
        return green;
    }
    void set_red(float value) {
        red = value;
    }
    void set_blue(float value) {
        blue = value;
    }
    void set_green(float value) {
        green = value;
    }
    Color & operator += (const Color &rhs) {
        red += rhs.red;
        blue += rhs.blue;
        green += rhs.green;
        return *this;
    }
    Color & operator -= (const Color &rhs) {
        red -= rhs.red;
        blue -= rhs.blue;
        green -= rhs.green;
        return *this;
    }
    Color & operator *= (const Color &rhs) {
        red *= rhs.red;
        blue *= rhs.blue;
        green *= rhs.green;
        return *this;
    }
    Color & operator *= (float scalar) {
        red *= scalar;
        blue *= scalar;
        green *= scalar;
        return *this;
    }
    Color & operator /= (float scalar) {
        red /= scalar;
        blue /= scalar;
        green /= scalar;
        return *this;
    }
    Color & trim(int order) {
        if (red > order) {
            red = order;
        }
        if (red < 0) {
            red = 0;
        }
        if (blue > order) {
            blue = order;
        }
        if (blue < 0) {
            blue = 0;
        }
        if (green > order) {
            green = order;
        }
        if (green < 0) {
            green = 0;
        }
        return *this;
    }
    bool operator == (const Color &rhs) {
        return red == rhs.red && blue == rhs.blue && green == rhs.green;
    }
};

Color operator + (const Color &lhs, const Color &rhs) {
    return {lhs.red + rhs.red,
            lhs.blue + rhs.blue,
            lhs.green + rhs.green};
}

Color operator - (const Color &lhs, const Color &rhs) {
    return {lhs.red - rhs.red,
            lhs.blue - rhs.blue,
            lhs.green - rhs.green};
}

Color operator * (const Color &lhs, const Color &rhs) {
    return {lhs.red * rhs.red,
            lhs.blue * rhs.blue,
            lhs.green * rhs.green};
}

Color operator * (const Color &lhs, float scalar) {
    return {lhs.red * scalar,
            lhs.blue * scalar,
            lhs.green * scalar};
}

Color operator * (float scalar, const Color &rhs) {
    return {rhs.red * scalar,
            rhs.blue * scalar,
            rhs.green * scalar};
}

Color operator / (const Color &lhs, float scalar) {
    return {lhs.red / scalar,
            lhs.blue / scalar,
            lhs.green / scalar};
}

ostream & operator << (ostream &out, const Color &rhs) {
    out << '('
        << rhs.red << ", "
        << rhs.blue << ", "
        << rhs.green << ')';
    return out;
}

#endif //RAYTRACER_COLOR_H
