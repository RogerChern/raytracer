### What is this repository for? ###

* This repo contains a toy raytracer which is mainly designed for polishing one's C++ skill.
* Now this project is in version 1.0, the first release which could render some very simple objects.
* The project is written reader-friendly, so the names are pretty self-explaning.

### How do I get set up? ###

* Since this is a very tiny project the classes used all go in their header files respectively.
* You can modify the configurations in the `main.cpp` file to create your own objects and viewpoints. Sorry for the lack of configure files support. I'll add it at a later time.
* Once you complete the configuration, you can simply compile the main.cpp file in any **C++11 compatible** compilers using `c++ main.cpp -std=c++11`

### Project structure guidelines ###

* The structure of the project is very simple. I just implemented the requirements of Caltech's C++ Advanced Track course.
  * the infrastructure class `Vector`, implemented using template, and instanciated a special version `using Vector3F = Vector<float, 3>`
  * the raytracing related class `Ray`, `Color`, `Camera`, `SceneObjects` and `Scene`.
    * `Ray` is what `Camera` emits.
    * `Color` is what raytracing returns.
    * `SceneObeject` is the base class of `Plane` and `Sphere`.
    * `Scene` is a container of `shared_ptr` to `SceneObjects`.