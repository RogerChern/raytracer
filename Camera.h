//
//  Camera.h
//  raytracer
//
//  Created by RogerChen on 5/13/15.
//  Copyright (c) 2015 RogerChen. All rights reserved.
//

#ifndef raytracer_Camera_h
#define raytracer_Camera_h

#include "Vector.h"

class Camera {
private:
    Vector3F location;
    Vector3F direction;
    Vector3F camera_up;
    Vector3F camera_right;
    float view_field; // in degree
    float distance;
public:
    Camera(const Vector3F &loc, const Vector3F &lookat, const Vector3F &up) :
            location(loc), direction((lookat - loc).normalized()) {
        camera_right = (direction % up).normalized();
        camera_up = (camera_right % direction).normalized();
        view_field = 60;
        distance = 0.5 / tanf(view_field / 2 * 3.14159265 / 180);
    }
    Ray get_ray_for_pixel(float x, float y, float image_size) const {
        Vector3F pixel_directoin = distance * direction + (0.5 - y / image_size) * camera_up + (x / image_size - 0.5) * camera_right;
        return {location, pixel_directoin};
    }
};

#endif
