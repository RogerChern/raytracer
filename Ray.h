//
// Created by RogerChern on 2015/5/11.
//

#ifndef RAYTRACER_RAY_H
#define RAYTRACER_RAY_H

// uncomment below to disable assert()
// #define NDEBUG
#include "Vector.h"
#include <assert.h>

class Ray {
private:
    Vector3F origin;
    Vector3F direction;
public:
    Ray(const Vector3F &orig, const Vector3F &dir) : Ray(orig, dir, true) {}
    Ray(const Vector3F &orig, const Vector3F &dir, bool normalize) : origin(orig), direction(dir) {
        if (normalize) {
            direction.normalize();
        }
    }
    Vector3F get_direction() const {
        return direction;
    }
    Vector3F get_origin() const {
        return origin;
    }
    Vector3F get_point_at_t(float time) const {
        assert(time >= 0);
        return origin + direction * time;
    }
    Ray get_refelect_ray(const Vector3F &n, const Vector3F &p) const {
        Vector3F refelect_direction = direction + 2 * project(-1 * direction, n);
        return {p + 0.0001 * refelect_direction, refelect_direction};
    }
};

#endif //RAYTRACER_RAY_H
