//
// Created by RogerChern on 2015/5/9.
//

#ifndef RAYTRACER_VECTOR_H
#define RAYTRACER_VECTOR_H

#include <iostream>
#include <math.h>
using std::ostream;

template <typename T> class Vector;

template <typename T>
inline Vector<T> operator + (const Vector<T> &lhs, const Vector<T> &rhs) {
    return {lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z};
}

template <typename T>
inline Vector<T> operator - (const Vector<T> &lhs, const Vector<T> &rhs) {
    return {lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z};
}

template <typename T>
inline float operator * (const Vector<T> &lhs, const Vector<T> &rhs) {
    return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
}

template <typename T>
inline Vector<T> operator * (const Vector<T> &vector, float scalar) {
    return {vector.x * scalar, vector.y * scalar, vector.z * scalar};
}

template <typename T>
inline Vector<T> operator * (float scalar, const Vector<T> &vector) {
    return {vector.x * scalar, vector.y * scalar, vector.z * scalar};
}

template <typename T>
inline Vector<T> operator / (const Vector<T> &vector, float scalar) {
    return {vector.x / scalar, vector.y / scalar, vector.z / scalar};
}

template <typename T>
Vector<T> operator % (const Vector<T> &lhs, const Vector<T> &rhs) {
    return {lhs.y * rhs.z - lhs.z * rhs.y,
            lhs.z * rhs.x - lhs.x * rhs.z,
            lhs.x * rhs.y - lhs.y * rhs.x};
}

template <typename T>
ostream & operator << (ostream &out, const Vector<T> &rhs) {
    out << "("
    << rhs.x << ", "
    << rhs.y << ", "
    << rhs.z << ")";
    return out;
}

template<typename T>
Vector<T> project(const Vector<T> &lhs, const Vector<T> &rhs) {
    return rhs * (lhs * rhs / rhs.squared_magnitude());
}

template <typename T>
class Vector {
friend Vector operator + <>(const Vector &lhs, const Vector &rhs);
friend Vector operator - <>(const Vector &lhs, const Vector &rhs);
friend float operator * <> (const Vector &lhs, const Vector &rhs);
friend Vector operator % <>(const Vector &lhs, const Vector &rhs); // operator % for cross product
friend ostream & operator << <>(ostream &out, const Vector &rhs);


friend Vector operator / <>(const Vector &vector, float scalar);
friend Vector operator * <>(const Vector &vector, float scalar);
friend Vector operator * <>(float scalar, const Vector &vector);
    
private:
    T x = 0.0;
    T y = 0.0;
    T z = 0.0;
public:
    Vector(T a, T b, T c) : x(a), y(b), z(c) {}
    Vector() = default;
    Vector(const Vector &) = default;
    Vector & operator = (const Vector &) = default;
    Vector & operator += (const Vector &rhs) {
        x += rhs.x;
        y += rhs.y;
        z += rhs.z;
        return *this;
    }
    Vector & operator -= (const Vector &rhs) {
        x -= rhs.x;
        y -= rhs.y;
        z -= rhs.z;
        return *this;
    }
    Vector & operator *= (T scalar) {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return *this;
    }
    Vector & operator /= (T scalar) {
        x /= scalar;
        y /= scalar;
        z /= scalar;
        return *this;
    }
    Vector operator - () {
        return {-x, -y, -z};
    }
    bool operator == (const Vector &rhs) {
        return rhs.x == x && rhs.y == y && rhs.z == z;
    }
    void set_x(T value) {
        x = value;
    }
    void set_y(T value) {
        y = value;
    }
    void set_z(T value) {
        z = value;
    }
    T get_x(T value) const {
        return x;
    }
    T get_y(T value) const {
        return y;
    }
    T get_z(T value) const {
        return z;
    }
    T magnitude() const {
        return sqrtf(squared_magnitude());
    }
    T squared_magnitude() const {
        return x * x + y * y + z * z;
    }
    void normalize() {
        T norm = magnitude();
        x /= norm;
        y /= norm;
        z /= norm;
    }
    Vector normalized() const {
        T norm = magnitude();
        return {x / norm, y / norm, z / norm};
    }
};

using Vector3F = Vector<float>;

#endif //RAYTRACER_VECTOR_H
