//
// Created by RogerChern on 2015/5/12.
//

#ifndef RAYTRACER_SCENE_H
#define RAYTRACER_SCENE_H

#include "Color.h"
#include "Vector.h"
#include "Ray.h"
#include "Camera.h"
#include <limits>
#include <vector>
#include <memory>
#include <iostream>

using std::vector;
using std::unique_ptr;
using std::shared_ptr;
using std::make_shared;
using std::ostream;

class SceneObject {
private:
    Color color;
    float reflectivity;
public:
    Color get_color() const {
        return color;
    }
    void set_color(const Color &value) {
        color = value;
    }
    float get_reflectivity() const {
        return reflectivity;
    }
    void set_reflectivity(float value) {
        reflectivity = value;
    }
    Color get_color_at(const Vector3F &point) const {
        return color;
    }
    // if a ray does not intersect with an object, return infty
    static constexpr float INFTY = std::numeric_limits<float>::infinity();
    virtual float get_t_intersection(const Ray &) const = 0;
    virtual Vector3F get_normal(const Vector3F &point) const = 0;
};

class Plane : public SceneObject {
private:
    float distance_to_origin;
    Vector3F normal;
public:
    Plane(float d, const Vector3F &n) : distance_to_origin(d), normal(n) {}
    float get_distance_to_origin() const {
        return distance_to_origin;
    }
    Vector3F get_normal() const {
        return normal;
    }
    Vector3F get_normal(const Vector3F &) const override {
        return normal;
    }
    float get_t_intersection(const Ray &r) const override {
        float t = -(distance_to_origin + r.get_origin() * normal) / (r.get_direction() * normal);
        if (!isnan(t) && !isinf(t) && t >= 0) {
            return t;
        }
        else {
            return INFTY;
        }
    }
};

class Sphere : public SceneObject {
private:
    Vector3F center;
    float radius;
    void get_intersection(const Ray &ray, float &t1, float &t2) const {
        float c2 = 1; // c2 = D^2 > 0, D is normalized
        float c1 = 2 * (ray.get_origin() * ray.get_direction() - ray.get_direction() * center); // c1 = 2(PD-DC)
        float c0 = ray.get_origin().squared_magnitude() + center.squared_magnitude() - 2 * ray.get_origin() * center - radius * radius; // c0 = P^2+C^2-r^2-2PC
        float discriminant = c1 * c1 - 4 * c0 * c2;
        if (discriminant > 0) {
            t1 = (-c1 - sqrtf(discriminant)) / (2 * c2);
            t2 = (-c1 + sqrtf(discriminant)) / (2 * c2);
        }
        else if (discriminant == 0) {
            t1 = -c1 / (2 * c2);
            t2 = INFTY;
        }
        else {
            t1 = t2 = INFTY;
        }
    }
public:
    Sphere(float r, const Vector3F &o) : radius(r), center(o) {}
    Vector3F get_center() const {
        return center;
    }
    float get_radius() const {
        return radius;
    }
    Vector3F get_normal(const Vector3F &p) const override {
        return (p - center).normalized();
    }
    float get_t_intersection(const Ray &ray) const override {
        float t1, t2;
        get_intersection(ray, t1, t2);
        if (t1 < 0) {
            if (t2 >= 0) {
                return t2;
            }
            else {
                return INFTY;
            }
        }
        else {
            return t1;
        }
    }
};

class Light {
private:
    Vector3F location;
    Color color;
public:
    Light(Vector3F o, Color c) : location(o), color(c) {}
    Color get_color() {
        return color;
    }
    Vector3F get_location() {
        return location;
    }
};

class Scene {
private:
    vector<shared_ptr<SceneObject>> objects;
    vector<shared_ptr<Light>> lights;
    shared_ptr<SceneObject> find_closest_object(const Ray &ray, float &t_intersection) const {
        shared_ptr<SceneObject> ret;
        float t;
        for (auto &&object : objects) {
            t = object->get_t_intersection(ray);
            if (t_intersection > t) {
                t_intersection = t;
                ret = object;
            }
        }
        return ret;
    }
public:
    Scene() = default;
    void add_scene_object(shared_ptr<SceneObject> p_obj) {
        assert(p_obj);
        objects.push_back(std::move(p_obj));
    }
    void add_light(shared_ptr<Light> p_light) {
        assert(p_light);
        lights.push_back(std::move(p_light));
    }
    Color trace_ray(const Ray &ray, int depth = 0) const {
        assert(!lights.empty() && !objects.empty());
        float t_intersection = SceneObject::INFTY;
        shared_ptr<SceneObject> closest_object = find_closest_object(ray, t_intersection);
        if (closest_object == nullptr) {
            return {0.05, 0.05, 0.05};
        }
        else {
            Vector3F intersection = ray.get_point_at_t(t_intersection);
            Color ret(0, 0, 0);
            for (auto &&light : lights) {
                ret += light->get_color() * closest_object->get_color() * fmaxf(closest_object->get_normal(intersection) * (light->get_location() - intersection).normalized(), 0);
            }
            if (depth < 3) {
                ret += closest_object->get_reflectivity() * trace_ray(ray.get_refelect_ray(closest_object->get_normal(intersection), intersection), depth + 1);
            }
            return ret;
        }
    }
    void render(const Camera &camera, float image_size, ostream &out) const {
        out << "P3 " << image_size << ' ' << image_size << ' ' << 255 <<  '\n';
        for (float y = 0; y < image_size; ++y) {
            for (float x = 0; x < image_size; ++x) {
                Ray pixel_ray = camera.get_ray_for_pixel(x, y, image_size);
                Color pixel_color = trace_ray(pixel_ray).trim(255);
                out << int(pixel_color.get_red() * 255) << ' '
                    << int(pixel_color.get_green() * 255) << ' '
                    << int(pixel_color.get_blue() * 255) << '\n';
            }
        }
    }
};

#endif //RAYTRACER_SCENE_H
